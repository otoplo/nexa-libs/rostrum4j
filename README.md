# Rostrum4j

Simple RPC client for rostrum project

## Installation

Look here https://gitlab.com/otoplo/nexa-libs/rostrum4j/-/releases for the latest package version.
In the pom.xml include the repository and dependency:
```
    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/44638913/packages/maven</url>
        </repository>
    </repositories>

    <dependency>
        <groupId>com.otoplo</groupId>
        <artifactId>rostrum4j</artifactId>
        <version>{VERSION}</version>
    </dependency>
```

## Usage
```
    public static void main(String[] args) throws Throwable {
        // Client init
        RostrumRpcClient client = new RostrumRpcClient("https://127.0.0.1/", 20001);

        try {
            client.openConnection();
            // operation
            Balance addresBalance = client.getBalance("nexa_addres");

            // raw operations with pojo in case the library not wrapped the required command
            Balance balance = client.executeRequest("getbalance", Balance.class, "nexa_address");

            // raw operations with generic type in case the library not wrapped the required command
            JsonNode json = client.executeRequest("getbalance", JsonNode.class, "nexa_address");
        } finally {
            client.closeConnection();
        }
    }
```
