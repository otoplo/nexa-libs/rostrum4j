package com.otoplo.rostrum4j;

import com.googlecode.jsonrpc4j.JsonRpcClient;
import com.otoplo.rostrum4j.model.*;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class RostrumRpcClient {

    private final String host;
    private final int port;

    private final JsonRpcClient client;

    private Socket socket;

    private OutputStream outputStream;


    public RostrumRpcClient(String host, int port) {
        this.host = host;
        this.port = port;
        client = new JsonRpcClient();
    }

    public void openConnection() throws IOException {
        socket = new Socket(host, port);
        outputStream = new ElectrumXOutputStream(socket.getOutputStream());
    }

    public void closeConnection() throws IOException {
        if (socket != null) {
            socket.close();
        }
    }

    public <T> T executeRequest(RostrumOps ops, Class<T> returnType, Object... params) throws Throwable {
        return executeRequest(ops.getMethod(), returnType, params);
    }

    public <T> T executeRequest(String method, Class<T> returnType, Object... params) throws Throwable {
        return client.invokeAndReadResponse(method, params, returnType, outputStream, socket.getInputStream());
    }

    public BlockTip getBlockTip() throws Throwable {
        return executeRequest(RostrumOps.GET_TIP, BlockTip.class);
    }

    public Balance getBalance(String address) throws Throwable {
        return executeRequest(RostrumOps.GET_BALANCE, Balance.class, address);
    }

    public Transaction getTransaction(String id) throws Throwable {
        return executeRequest(RostrumOps.GET_TX, Transaction.class, id, true);
    }

    public String getTransactionHex(String id) throws Throwable {
        return executeRequest(RostrumOps.GET_TX, String.class, id, false);
    }

    public TxHistory[] getTransactionsHistory(String address) throws Throwable {
       return executeRequest(RostrumOps.GET_HISTORY, TxHistory[].class, address);
    }

    public UTXO[] getListUnspent(String address) throws Throwable {
        return executeRequest(RostrumOps.LIST_UNSPENT, UTXO[].class, address);
    }

    public UTXOInfo getUTXOInfo(String outpoint) throws Throwable {
        return executeRequest(RostrumOps.GET_UTXO_INFO, UTXOInfo.class, outpoint);
    }
}
