package com.otoplo.rostrum4j.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
public class Transaction {

    private String blockHash;
    private long blockTime;
    private long confirmations;
    private long height;
    private String hash;
    private String txId;
    private String txIdem;
    private String hex;
    private long lockTime;
    private int size;
    private long time;
    private int version;

    @JsonProperty("vin")
    private List<TxInput> inputs;

    @JsonProperty("vout")
    private List<TxOutput> outputs;
}
