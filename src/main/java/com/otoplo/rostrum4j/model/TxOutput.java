package com.otoplo.rostrum4j.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
public class TxOutput {

    @JsonProperty("n")
    private int pos;

    private int type;

    private ScriptPubKey scriptPubKey;

    private BigDecimal value;

    @JsonProperty("value_satoshi")
    private BigDecimal satoshis;
}
